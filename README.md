# Programmation système INFO-F201
Projet : Création d'un salon de chat

**Auteurs** :

*Liam FALLIK*

*David TOUCHE*

### Installation : (terminal)

Téléchargement du projet : 
```git clone https://gitlab.com/Liamfallik/programmation-systeme-info-f201.git```

Compilation : 
```make```

Lancement du serveur : ```./server [port]```

Lancement du client : ```./client [pseudo] [ip] [port]```

