/***********************************************************************
* 																	   *
*       common.h - Fonctions communes au client et au serveur 		   *
*                 Liam FALLIK et David TOUCHE                          *
*                                                                      *
***********************************************************************/

#ifndef __COMMON_H
#define __COMMON_H

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <string.h>

#define TSIZE sizeof(time_t)
#define MSIZE sizeof(size_t)


int _checked(int ret, char* calling_function) {
	if (ret < 0) {
		perror(calling_function);
		exit(EXIT_FAILURE);
	}
	return ret;
}


// The macro allows us to retrieve the name of the calling function
#define checked(call) _checked(call, #call)

/**
 * Envoi d'un paquet sous la forme <longueur><timestamp><message>.
 */
void ssend(int sock, void* data, size_t len) {
	checked(write(sock, &len, sizeof(len)));
	time_t now = time(NULL);
	checked(write(sock, &now, sizeof(time_t)));
	checked(write(sock, data, len));
}

/** Fonction receive
 * Permet de recevoir les données concernant le message, à savoir
 * sa longueur, le timestamp et le message lui-même.
 * Paramètres : 
 * - int sock : socket à lire
 * - void** dest : pointeur d'un espace mémoire où l'on va copier le message
 * - time_t* timeptr : pointeur où on va copier la valeur du temps
 * 					   (on ne peut renvoyer plusieurs valeurs d'une fonction
 * 					   en C, ceci est une astuce permettant le même résultat)
 * Valeur de retour : 
 * - size_t total_received : taille du message reçu
 */
size_t receive(int sock, void** dest, time_t* timeptr) {
	// Réception de la longueur du message
	size_t nbytes_to_receive;
	if (checked(read(sock, &nbytes_to_receive, sizeof(nbytes_to_receive))) == 0) {
		// Longueur du message = 0 -> déconnexion
		return 0;
	}
	
	// Réception du temps
	checked(read(sock, timeptr, sizeof(time_t)));
	
	// Réception du message, allocation d'un espace de taille nbytes_to_receive
	unsigned char* buffer = malloc(nbytes_to_receive);
	if (buffer == NULL) {
		fprintf(stderr, "malloc could not allocate %zd bytes", nbytes_to_receive);
		perror("");
		exit(1);
	}
	// Si le message est reçu par paquets, on attend de recevoir tout le message
	size_t total_received = 0;
	size_t received = 1;
	while (nbytes_to_receive > 0 && received > 0) {
		received = checked(read(sock, &buffer[total_received], nbytes_to_receive));
		total_received += received;
		nbytes_to_receive -= received;
	}
	// La ligne ci-dessous évite d'avoir des caractères indésirables à la fin du message
	buffer[total_received] = '\0';
	*dest = buffer;
	return total_received;
}

/** Fonction getTime
 * Renvoi d'une chaîne de caractères de format [HH:MM:SS] représentant
 * le moment où le message à été envoyé à partir du time_t
 * Paramètres :
 * - char* raw : paquet reçu
 */
char* getTime(time_t recvtime) {
	char* timestr = malloc(12*sizeof(char));
	struct tm *tm_struct = localtime(&recvtime);
	sprintf(timestr, "[%02i:%02i:%02i]", tm_struct->tm_hour, tm_struct->tm_min, tm_struct->tm_sec);
	return timestr;
}

#endif  // __COMMON_H
