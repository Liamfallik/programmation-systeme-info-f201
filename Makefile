all: server client
server: server.c common.h
	gcc -Wall -o server server.c common.h
client: client.c common.h
	gcc -Wall -o client client.c common.h -pthread

