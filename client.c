/***********************************************************************
* 																	   *
*                  client.c - Code du client                           *
*                 Liam FALLIK et David TOUCHE                          *
*                                                                      *
***********************************************************************/

#include <arpa/inet.h>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
#include <pthread.h>
#include "common.h"
#include <time.h>
#include <stdbool.h>

/** Struct thread_args_t
 * Arguments à passer à la fonction créant les threads, celle-ci 
 * n'acceptant qu'un seul paramètre.
 * Membres :
 * - int* socket : numéro du socket correspondant au client
 * - char* pseudo : Pseudo choisi par l'utilisateur
 */
typedef struct {
  int* socket;
  char* pseudo;
} thread_args_t;

/** Fonction receive_thread_function
 * Fonction du thread qui s'occupe de la réception et de l'affichage 
 * des messages dans le terminal
 * Paramètre :
 * - void* ptr : pointeur vers le struct contenant les arguments
 */
void* receive_thread_function(void* ptr){
	thread_args_t* args = (thread_args_t*)ptr;
    int* socket_ptr= args->socket;
	ssize_t nbytes = 1;
	time_t rtime;
	printf("Initialisation de la réception\n");
	int client_socket = *((int *)socket_ptr);
	while (true) {
		if (nbytes > 0) {
			char *pseudo;
			char *recvbuffer;
			nbytes = receive(client_socket, (void *)&pseudo, &rtime);
			nbytes = receive(client_socket, (void *)&recvbuffer, &rtime);
			if (nbytes > 0) {
				char *time = getTime(rtime);
				printf("%s %s : %s\n", time, pseudo, recvbuffer);
				free(time);
				free(recvbuffer);
				free(pseudo);
			}
			else if (nbytes <= 0){
				printf("Le serveur est déconnecté. Appuyez sur ENTREE pour fermer le client\n");
				getchar();
				free(recvbuffer);
				free(pseudo);
				close(client_socket);
				exit(0);
			}
		}
	}
}

/** Fonction send_thread_function
 * Fonction du thread qui s'occupe de l'envoi des messages dans le 
 * bon format (longeur|temps|message)
 * Paramètre :
 * - void* ptr : pointeur vers le struct contenant les arguments
 */
void* send_thread_function(void* ptr) {
  printf("Initialisation de l'envoi\n");
  thread_args_t* args = (thread_args_t*)ptr;
  int* socket_ptr= args->socket;
  char buffer[1024];
  memset(buffer, '\0', 1024);
  ssize_t nbytes = 1;
  int client_socket = *((int *)socket_ptr);
  while (nbytes > 0 && fgets(buffer, 1024, stdin)) {
	size_t nbytes = strlen(buffer);
	ssend(client_socket, buffer, nbytes);
	memset(buffer, '\0', 1024);
  }
  close(client_socket);
  printf("Déconnexion du client en cours \n");
  exit(0);
}

/** Arguments :
 * - [1] : pseudo
 * - [2] : adresse ip
 * - [3] : port
 */
int main(int argc, char const *argv[]) {
  if(argc < 4) {
	  printf("Lancement du client : ./client [pseudo] [adresse-ip] [port]\n");
	  exit(1);
  }
  if(strlen(argv[1]) > 20) {
	  printf("Le pseudo ne peut pas être plus long que 20 caractères.\n");
	  exit(1);
  }
  
  printf("Bienvenue %s! Connexion à %s:%s en cours\n", argv[1],argv[2],argv[3]);
  
  int sock = checked(socket(AF_INET, SOCK_STREAM, 0));
  struct sockaddr_in serv_addr;
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_port = htons(atoi(argv[3]));
  // Conversion de string vers IPv4 ou IPv6 en binaire
  checked(inet_pton(AF_INET, argv[2], &serv_addr.sin_addr));
  // Connexion au serveur
  checked(connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)));
  // On est connecté et on envoie le pseudo
  char *pseudo = malloc(sizeof(char) * (strlen(argv[1])) + 1);
  sprintf(pseudo, "%s", argv[1]);
  // La ligne ci-dessous évite d'avoir des caractères indésirables à la fin du message
  pseudo[strlen(argv[1])] = '\0';
  ssend(sock, pseudo, strlen(pseudo));
  printf("Connexion établie !\n");
  
  thread_args_t args;
  
  //Initialisation des arguments du thread
  args.socket = &sock;
  args.pseudo=pseudo;
  
  //Lancement des threads pour recevoir ou envoyer;
  pthread_t tids[3];
  pthread_create(&tids[0], NULL, receive_thread_function, &args);
  pthread_create(&tids[1], NULL, send_thread_function, &args);
  pthread_join(tids[0], NULL);
  pthread_join(tids[1], NULL);

  free(pseudo);
  return 0;
}

