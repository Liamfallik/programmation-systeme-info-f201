/***********************************************************************
* 																	   *
*                  server.c - Code du serveur                          *
*                 Liam FALLIK et David TOUCHE                          *
*																	   *
***********************************************************************/

#include <netinet/in.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>  //strlen
#include <sys/socket.h>
#include <sys/time.h>  //FD_SET, FD_ISSET, FD_ZERO macros
#include <unistd.h>    //close
#include <signal.h>
#include <time.h>
#include <arpa/inet.h>

#include "common.h"

/** @struct Client
 * Permet de stocker les différents identifiants de chaque client dans la même liste
 */
typedef struct Client{
	char pseudo[20];
	int fd;
} Client;

/** Initialisation de la liste des clients */
Client clients[1024];
int nclients = 0;

/** @function signalHandler
 * Fonction qui s'active lors de la réception du signal SIGINT
 * (lorsqu'on appuye sur CTRL+C)
 */
void signalHandler(int signum) {
	if(signum == SIGINT) { // SIGINT = 2
		printf("\nReception du signal %i. Fermeture du serveur pour %i client(s)...\n", signum, nclients);
		for(int i = 0; i < nclients; i++){
			close(clients[i].fd);
		}
		exit(signum);
	}
}

/** Fonction signalIgnore
 * Permet d'ignorer le SIGPIPE qui s'envoie si l'on essaie d'écrire
 * dans un socket fermé et évite la fermeture du serveur
 */
void signalIgnore(int signum) {
	printf("Réception du signal %i - écriture dans un mauvais socket\n", signum);
}

/** Fonction messageEveryone
 * Envoie le message à tous les clients actuellement connectés
 * Paramètres :
 * - int sendingfd : numéro du fd du client qui a envoyé le message
 * - char message[] : le message à envoyer
 */
void messageEveryone(int sendingfd, char message[]) {
	size_t nbytes = strlen(message);
	for(unsigned int i = 0; i < nclients; i++) {
		// Si l'on souhaite ne pas renvoyer le message au client qui l'a
		// envoyé :
		// if(clients[i].fd != sendingfd) {
		if(1) {
			ssend(clients[i].fd, message, nbytes);
		}
	}
}

/** Fonction messageEveryoneDisconnect
 * Envoi d'un message de déconnexion à tous les clients restants, 
 * Paramètres :
 * - int sendingfd : numéro du fd du client qui a envoyé le message
 *                   (le plus souvent - master_socket : serveur)
 * - char message[] : le message à envoyer
 * - int dcfd : numéro du file descriptor du client qui s'est déconnecté
 * 				(pour ne pas lui envoyer de message)
 */
void messageEveryoneDisconnect(int sendingfd, char message[], int dcfd) {
	size_t nbytes = strlen(message);
	for(unsigned int i = 0; i < nclients; i++) {
		if(clients[i].fd != sendingfd && clients[i].fd != dcfd) {
			ssend(clients[i].fd, message, nbytes);
		}
	}
}

/** Arguments :
 * - [1] : port
 */
int main(int argc, char *argv[]) {
	// Lancement du serveur
	int port;
	if(argc < 2) {
		printf("Pas de numéro de port spécifié. Lancer le serveur avec un port par défaut ? [y/n] : ");
		char ans;
		scanf(" %c", &ans);
		if(ans == 'y' || ans == 'Y') {
			port = 8000; // Port par défaut
		}
		else if(ans == 'n' || ans == 'N') {
			printf("Fermeture du programme.\n");
			exit(1);
		}
		else { printf("Réponse invalide, fermeture du programme.\n"); exit(1); }
	}
	else if(argc >= 2) {
	port = atoi(argv[1]);
	}
  
	int opt = 1;
	int master_socket = checked(socket(AF_INET, SOCK_STREAM, 0));
	checked(setsockopt(master_socket, SOL_SOCKET, SO_REUSEADDR, (char *)&opt, sizeof(opt)));

	// Création du socket
	struct sockaddr_in address;
	address.sin_family = AF_INET;
	address.sin_addr.s_addr = INADDR_ANY;
	address.sin_port = htons(port);
	checked(bind(master_socket, (struct sockaddr *)&address, sizeof(address)));
	checked(listen(master_socket, 3));

	size_t addrlen = sizeof(address);
	fd_set readfds;
 
	// Réception des signaux
	signal(SIGINT, signalHandler);
	signal(SIGPIPE, signalIgnore);

	printf("Serveur lancé sur l'addresse %s:%i !\n", inet_ntoa(address.sin_addr), port);

	time_t rtime;
	
	// Initialisation des strings pour la comparaison des commandes
	
	char * helpcmd = "/hlp\0";
	char * lstcmd = "/lst\0";
	char * lstmsg = "Utilisateurs connectés :";
	char * helpmsg = "Commandes disponibles :\n/hlp - affiche le menu d'aide\n/lst - affiche la liste des utilisateurs connectés";
	char * unknownmsg = "Commande non reconnue.\nTapez /hlp pour une liste des commandes.";
	char first;
	
	// Fonctionnement du serveur
	while (true) {
		FD_ZERO(&readfds);
		FD_SET(master_socket, &readfds);
		int max_fd = master_socket;
		for (int i = 0; i < nclients; i++) {
			FD_SET(clients[i].fd, &readfds);
			if (clients[i].fd > max_fd) {
				max_fd = clients[i].fd;
			}
		}
		// Attente d'activité (écriture) sur un fd
		int activity = select(max_fd + 1, &readfds, NULL, NULL, NULL);
		if ((activity < 0)) {
			printf("Select Error");
		}

		if (FD_ISSET(master_socket, &readfds)) {
			// Si c'est le master socket qui a des données, c'est une nouvelle connexion.
			clients[nclients].fd = accept(master_socket, (struct sockaddr *)&address, (socklen_t *)&addrlen);
			// Récupération du pseudo pour stockage
			char *pseudo;
			ssize_t nbytes = receive(clients[nclients].fd, (void*)&pseudo, &rtime);
			int loop = 1;
			while(loop) {
				if(nbytes > 0) {
					strcpy(clients[nclients].pseudo, pseudo);
					loop = 0;
				}
			}
			printf("[+] Nouvelle connexion - %s!\n", clients[nclients].pseudo);
			free(pseudo);
			
			char* buffer = malloc(sizeof(char) * (40 + strlen(clients[nclients].pseudo)));
			sprintf(buffer, "Nouvelle connexion - %s!\n", clients[nclients].pseudo);
			
			messageEveryone(master_socket, "SERVEUR");
			messageEveryone(master_socket, buffer);
	  
			nclients++;
			free(buffer);
		} 
		else {
			// Si l'activité n'est pas sur le master_socket, c'est le message d'un client,
			// l'on doit lire les fd de tous les clients connectés pour voir
			// lequel a envoyé un message
			for (int i = 0; i < nclients; i++) {
				if (FD_ISSET(clients[i].fd, &readfds)) {
					char *buffer;
					size_t nbytes = receive(clients[i].fd, (void*)&buffer, &rtime);
					if (nbytes > 0) {  // On a reçu un message
						
						// Test pour voir si il s'agit d'une commande : le message commence par un "/"
						first = buffer[0];
						if(first == '/') {
							char* cmp = malloc(sizeof(char)*4);
							strncpy(cmp, buffer, 4);
							if (strcmp(cmp, lstcmd) == 0) {
								// Affichage de la liste
								ssend(clients[i].fd, "CONSOLE", 8);
								ssend(clients[i].fd, lstmsg, strlen(lstmsg));
								for(unsigned int j = 0; j < nclients; j++) {
									ssend(clients[i].fd, "CONSOLE", 8);
									ssend(clients[i].fd, clients[j].pseudo, strlen(clients[j].pseudo));
								}
							}
							else if (strcmp(cmp, helpcmd) == 0) {
								// Affichage du message d'aide
								ssend(clients[i].fd, "CONSOLE", 8);
								ssend(clients[i].fd, helpmsg, strlen(helpmsg));
							}
							else {
								// Affichage du message d'erreur
								ssend(clients[i].fd, "CONSOLE", 8);
								ssend(clients[i].fd, unknownmsg, strlen(unknownmsg));
							}
							free(cmp);
						}
						else {
							// Renvoyer le message à tous les sockets (sauf à celui qui a envoyé le message - non implémenté)
							messageEveryone(clients[i].fd, clients[i].pseudo);
							messageEveryone(clients[i].fd, buffer);
						}
						free(buffer);
            
					} else { // Message vide - le client s'est déconnecté
						char *buffer = malloc(sizeof(char) * 1024);
						printf("[-] %s s'est déconnecté(e).\n", clients[i].pseudo);
						sprintf(buffer, "%s s'est déconnecté(e).\n", clients[i].pseudo);
			  
						messageEveryoneDisconnect(master_socket, "SERVEUR", clients[i].fd);
						messageEveryoneDisconnect(master_socket, buffer, clients[i].fd);
              
						// Fermeture du client
						close(clients[i].fd);
						
						// On deplace le dernier socket a la place de libre pur ne pas faire de trou.
						clients[i] = clients[nclients - 1];
						nclients--;
						free(buffer);
					}
				}
			}
		}
	}
	return 0;
}
